# Pipeline to be run once a commit is marked for release (using a tag).
# Pipeline assumes there is an image with a short git commit sha.
# The image get another image-tag equals to the commit-tag.
# Infrastructure repository updated so that overlay corresponding to
# the tag is updated to refer to the image released.
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: aardvark-release-pipeline
spec:
  workspaces:
  - name: infra-workspace
  - name: dummy-workspace
  # Need for dummy-workspace shall hopefully be removed, once git-cli and skopeo-copy support optional ws.
  params:
  - name: git-repo-url
    type: string
    description: url of the git repo for the code
  - name: gitlab-repository-full-name
    type: string
    description: |
      The GitLab repository full name, e.g.: arbetsformedlingen/devops/aardvark-demo
  - name: git-tag
    type: string
    description: tag set on the commit
  - name: git-commit
    description: sha of commit to build
  - name: git-commit-short
    description: short sha of commit marked for release
  - name: release-user
    type: string
    description: user name of user doing the release.
    default: "unknown"
  - name: image
    type: string
    description: image to be released, excluding tag. Tag is deducted from git-commit-short.
  - name: git-infra-repo-url
    type: string
    description: url of the git repo for infrastructure code, will be patched to match build image
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: gitlab-run-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-release"
    - name: STATE
      value: running
    - name: DESCRIPTION
      value: Build has been triggered and is building.
  - name: tag-image-with-git-tag
    taskRef:
      name: skopeo-copy
      kind: Task
    params:
    - name: srcImageURL
      value: docker://$(params.image):$(params.git-commit-short)
    - name: destImageURL
      value: docker://$(params.image):$(params.git-tag)
    workspaces:
    - name: images-url
      workspace: dummy-workspace
    runAfter:
    - gitlab-run-status
  - name: check-infra-repo
    when:
    - input: "$(params.git-tag)"
      operator: in
      values:
      - prod
      - prod-stdby
      - staging
      - test
      - i1
      - t2
      - onprem
    taskRef:
      name: repo-exist
    params:
    - name: repo
      value: $(params.git-infra-repo-url)
    runAfter:
    - gitlab-run-status
  - name: fetch-infra-repository
    when:
    - input: "$(tasks.check-infra-repo.results.repo-exist)"
      operator: in
      values:
      - "true"
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: infra-workspace
    params:
    - name: url
      value: $(params.git-infra-repo-url)
    - name: subdirectory
      value: ""
    - name: deleteExisting
      value: "true"
    - name: submodules
      value: "false"
    runAfter:
    - check-infra-repo
  - name: set-sha
    taskRef:
      name: jobtech-yq
      kind: ClusterTask
    workspaces:
    - name: source
      workspace: infra-workspace
    params:
    - name: expression
      value: .images[].newTag="$(params.git-commit-short)"
    - name: file
      value: "kustomize/overlays/$(params.git-tag)/kustomization.yaml"
    runAfter:
    - fetch-infra-repository
  - name: push-infra-repository
    taskRef:
      name: git-cli
      kind: Task
    workspaces:
    - name: source
      workspace: infra-workspace
    - name: input
      workspace: dummy-workspace
    params:
    - name: GIT_USER_EMAIL
      value: "Builder.Bob@arbetsformedlingen.se"
    - name: GIT_USER_NAME
      value: "Builder Bob"
    - name: GIT_SCRIPT
      value: |
        git diff
        git commit -a -m "Marked git commit $(params.git-commit-short) for release to $(params.git-tag). The release was made by $(params.release-user)."
        git push origin HEAD:main
    runAfter:
    - set-sha
    - tag-image-with-git-tag
  finally:
  - name: gitlab-success-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-release"
    - name: STATE
      value: success
    - name: DESCRIPTION
      value: Build has been finalized with success.
  - name: gitlab-failed-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Failed"
      - "None"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-release"
    - name: STATE
      value: failed
    - name: DESCRIPTION
      value: Build failed. Check logs for details.
