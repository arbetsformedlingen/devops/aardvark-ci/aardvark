# Utils

This directory contains utilities to support usages of Aardvark.

## aardvark-diff.sh

Compare current branch with main or master on how it impact each environment.

Currently tested on Ubuntu Linux. It will probably work in WSL on Windows. It is also
high chance it work on MacOS.

`kubectl` and `git` must be installed.

### Usage

Go to your infra-repository. Checkout the branch you are working on and verify no
uncommited or untracked files exist.

Run `aardvark-diff.sh` without any arguments.

The following output example shows there are differences between main
branch and the `revert-1-proc-pod` branch for the develop and personal
environment. The other are the same.

```text
Output directory: diff-ZeFP
Switched to branch 'main'
Your branch is up to date with 'origin/main'.
Already up to date.
Switched to branch 'revert-1-proc-pod'


develop: Difference between main and revert-1-proc-pod branch. See diff in diff-ZeFP/diff/develop.yaml
i1: No change between main and revert-1-proc-pod branch.
personal: Difference between main and revert-1-proc-pod branch. See diff in diff-ZeFP/diff/personal.yaml
prod: No change between main and revert-1-proc-pod branch.
prod-stdby: No change between main and revert-1-proc-pod branch.
staging: No change between main and revert-1-proc-pod branch.
t2: No change between main and revert-1-proc-pod branch.
```

If there are differences in any environment `aardvark-diff.sh` has
created a temporary directory named `diff-XXXX`, where XXXX differs
for each run. In the example above `diff-ZeFP`.
In this directory one can find three subdirectories:

* `main` - containing output for each environment on the default branch.
* `branch` - containing output for each environment on the working branch.
* `diff` - diff output for each environment that differ between default and working branch.

These files is useful to further investigate the differences.

### Behind the scene

`aardvark-diff.sh` first verifies the current directory is git-managed,
has the Aardvark expected file structure and ensures no uncommited or
untracked files exist. Those files might cause trouble.

A temporary directory is created.

`aardvark-diff.sh` run kustomize for each directory in `kustomize/overlays`
on the branch. It stores one output file for each environment under
`temporary dir/branch`.

The default branch (main or master) is checked out and changes are
pulled from origin. If both branches exist, main is used.

`aardvark-diff.sh` runs once again kustomize for each directory
in `kustomize/overlays` on the default branch. It stores one
output file for each environment under `temporary dir/main`.

`aardvark-diff.sh` checks out the branch we started at.

Finally the output of kustomize on the default and working branch
are compared. Differences are stored in `temporary dir/diff`.
For environments that are unchanged, no diff-file is created.

If differences where found the temporary dir is kept to ease
further comparisons.
